# <img src="https://raw.githubusercontent.com/ayushsnha/hellow-world/master/68747470733a2f2f6d75736573636f72652e6f72672f73697465732f6d75736573636f72652e6f72672f66696c65732f4361707475726525323064253237652543432538316372616e253230323031362d30332d303125323030392e34382e31315f302e706e.png" align="center"/>

## Aossie-Scholar web app for Aossie (GSOC 2019) By Manikaran Singh

# Aossie-Scholar

The project is related to Google Scholar profiles and metrics. Many researchers have a Google Scholar profile. 
It is used by people to see how many papers a researcher has written, how many citations they have received, their h-index, i10-index... 
But these metrics are flawed. The goal of the project would be to extract information from Google Scholar and compute better metrics about a researcher's performance.
And then display this information and metrics with more fairer stats in another website.

## To run the app locally,
    # git remote add origin https://gitlab.com/aossie/aossie-scholar.git
    # git pull origin GSoC_2019
    # pip install -r requirements.txt
    # python manage.py runserver
    
   Now, you should be able to see the app running on your local server here -http://127.0.0.1:8000/metrics/
   One of the User Profile here- http://127.0.0.1:8000/metrics/m8dFEawAAAAJ/results/
